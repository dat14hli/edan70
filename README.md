# EDAN70

Project in Computer Science, EDAN70, LLVM Optimization Passes Ordering


## Config file
To change optimization mode (manual, brute_force, preset and rand_search),
change the opt_mode value in the config file.

A generic config file config.txt can be found in the src/ folder. Other
benchmark specific config files can be found in respective benchmark folder.
The current configuration files uses the transformation passes that corresponds
to -O3. These can be changed if you want a smaller test case.

### Optimization modes

#### manual
The optimizations will be applied in the order specified in the config file
under the key passes.

#### brute_force
The program will evaluate all possible optimization orders for the optimizations
specified in the config file under the key passes.

#### rand_search
Consists of two stages. The first stage will test randomized optimization orders
n1 times. The second stage will take the best order from stage one and randomly
swap the order of two optimizations until no improvement has been observed after
n2 times. The n1 and n2 value can be changed with the stage1_iter and
stage2_iter key.

#### preset
Ignores any optimizations specified in the config file. Uses LLVM default -O0,
-O1, -O2 or -O3 optimizations. The optimization level can be changed with the
opt_level key.

### Optimization pass configuration
The desired optimizations can be specified under the passes key,

### Other configuration
Other more advanced configuration can be done in the config file, in most cases
these should not be needed to change.

## Test runs
The following test was done in the report:

`./passes.py -i benchmarks/vcc/a.c -c benchmarks/vcc/config.json`
`./passes.py -i benchmarks/project_euler/273.cpp -c benchmarks/project_euler/config.json -s 30`
`./passes.py -i benchmarks/project_euler/324.cpp -c benchmarks/project_euler/config.json -s 3`
`./passes.py -i benchmarks/project_euler/60.cpp -c benchmarks/project_euler/config.json -s "700 4"`
`./passes.py -i benchmarks/project_euler/304.cpp -c benchmarks/project_euler/config.json -s "3 10 100"`
`./passes.py -i benchmarks/project_euler/80.cpp -c benchmarks/project_euler/config.json -s "2 100"`
`./passes.py -i benchmarks/project_euler/126.cpp -c benchmarks/project_euler/config.json -s 10`

Both configuration files have 40 stage 1 iterations and 20 stage 2 iterations.
The second configuration file used for the Project Euler test cases differs by
using the flag `-std=c++11` for Clang.

## Credits
The files in following directories in the repository come from:

- src/benchmarks/project_euler: Solutions to Project Euler programming problems provided by Stephan Brumme.
- src/benchmarks/vcc: Test program bundled with the vcc c compiler by Jonas Skeppstedt.

## License
This repository is covered by the license BSD 2-clause, see file LICENSE.
