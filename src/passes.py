#!/usr/bin/python3

from itertools import permutations
from random import shuffle
from collections import Counter, OrderedDict
from subprocess import PIPE, run
import datetime
import math
import random
import argparse
import operator
import re
import json
import sys


def generateIR(input, flags, output):
    print('Generating LLVM IR...')
    clang = ['clang', input] + flags + ['-o', output]
    result = run(clang, stdout=PIPE, stderr=PIPE, universal_newlines=True)
    if result.returncode != 0:
        print(result.stderr)
        exit(1)


def runOptimization(file, flags, passes):
    print('Running optimizations pass...')
    for opt in passes:
        if opt == '' or opt == '-O0':
            continue
        print('    Running pass ' + opt)
        opt = ['opt', file] + flags + [opt] + ['-o', file]
        result = run(opt, stdout=PIPE, stderr=PIPE, universal_newlines=True)
        if result.returncode != 0:
            print(result.stderr)
            exit(1)


def createObjectFiles(input, flags, output):
    print('Creating object files...')
    llc = ['llc', input] + flags + ['-o', output]
    result = run(llc, stdout=PIPE, stderr=PIPE, universal_newlines=True)
    if result.returncode != 0:
        print(result.stderr)
        exit(1)


def linker(input, flags, output):
    print('Running linker...')
    linker = ['g++', input] + flags + ['-o', output]
    result = run(linker, stdout=PIPE, stderr=PIPE, universal_newlines=True)
    if result.returncode != 0:
        print(result.stderr)
        exit(1)


def execute(input, flags, stdin, prog_flags):
    print('Executing with valgrind...')
    run(['chmod', 'u+x', input])
    valgrind = ['valgrind'] + flags + ['./' + input] + [prog_flags]
    print('    Equivalent executed command $ echo ' + stdin + ' | ./' + input + ' ' + prog_flags)
    result = run(valgrind, stdout=PIPE, stderr=PIPE, input=stdin, universal_newlines=True)
    if result.returncode != 0:
        print(result.stderr)
        exit(1)

    exe_instr = r'I\s+refs:\s+([\d|,]+)'
    instr = re.search(exe_instr, result.stderr).group(1)
    instr = instr.replace(',', '')
    print('    Executed instruction count: ' + instr)
    print('    Progam output:')
    print(result.stdout)
    return int(instr)


def printInstructionCount(input):
    result = run(['objdump', '-d', input], stdout=PIPE, stderr=PIPE, universal_newlines=True)
    instr_stat = r'\w+:\t[\w\s]+\t(\w+)'
    result = re.findall(instr_stat, result.stdout)
    instr_count = len(result)
    print('    Static instruction count: ' + str(instr_count))
    print('    Instruction statistics:')
    print('        Opcode     Count      Percentage (%)')
    result = Counter(result)
    result = OrderedDict(result.most_common())
    for instr in result:
        percentage = round(result[instr] / instr_count * 100, 2)
        line = [instr, str(result[instr]), str(percentage)]
        print('        {: <10} {: <10} {: <10}'.format(*line))


def main():
    parser = argparse.ArgumentParser(description='LLVM optimizations passes ordering')
    parser.add_argument('-i','--input', help='The source code to be compiled', required=True)
    parser.add_argument('-c','--config', help='Config file for compiler', required=False, default='config.json')
    parser.add_argument('-s','--stdin', help='Stdin for program execution equivalent to running echo hello | ./a', required=False, default='')
    parser.add_argument('-a','--args', help='Argument for program execution', required=False, default='')
    args = vars(parser.parse_args())

    with open(args['config']) as conf:
        config = json.load(conf)

    config['valgrind']['stdin'] = args['stdin']
    config['valgrind']['args'] = args['args']

    start = datetime.datetime.now()

    generateIR(args['input'], config['clang']['flags'], config['clang']['output'])
    if config['opt_mode'] == 'manual':
        manual(config, True)
    elif config['opt_mode'] == 'brute_force':
        bruteForce(config)
    elif config['opt_mode'] == 'rand_search':
        randomSearch(config)
    elif config['opt_mode'] == 'preset':
        preset(config)

    end = datetime.datetime.now()
    elapsed = end - start
    print('Approximate compile time (HH:mm:ss.SSSSSS): ' + str(elapsed))


def manual(config, printInstr):
    run(['cp', config['clang']['output'], config['opt']['output']])
    runOptimization(config['opt']['output'], config['opt']['flags'], config['opt']['passes'])
    createObjectFiles(config['opt']['output'], config['llc']['flags'], config['llc']['output'])
    linker(config['llc']['output'], config['g++']['flags'], config['g++']['output'])
    if printInstr:
        printInstructionCount(config['g++']['output'])
    return execute(config['g++']['output'], config['valgrind']['flags'], config['valgrind']['stdin'], config['valgrind']['args'])


def bruteForce(config):
    pass_rank = []
    print('Brute forcing ' + str(math.factorial(len(config['opt']['passes']))) + ' combinations')
    for combo in permutations(config['opt']['passes']):
        pass_order = list(combo)
        config['opt']['passes'] = pass_order
        instr = manual(config, False)
        pass_rank += [{'instr_count' : instr, 'pass_order' : pass_order}]

    pass_rank.sort(key=operator.itemgetter('instr_count'))
    print('Optimization pass orders')
    print('    Instructions     Order')
    for rank in pass_rank:
        line = [str(rank['instr_count']), str(rank['pass_order'])]
        print('    {: <16} {: <10}'.format(*line))

    print('Executing best order and printing instruction statistics')
    config['opt']['passes'] = pass_rank[0]['pass_order']
    manual(config, True)


def randomSearch(config):
    best_order = config['opt']['passes']
    instructions = sys.maxsize
    print('Stage 1, testing ' + str(config['opt_mode_conf']['rand_search']['stage1_iter']) + ' random orders')
    for _ in range(config['opt_mode_conf']['rand_search']['stage1_iter']):
        pass_order = list(best_order)
        shuffle(pass_order)
        config['opt']['passes'] = pass_order
        instr = manual(config, False)
        if instructions > instr:
            best_order = pass_order
            instructions = instr

    iterations = 0
    print('Stage 2, swapping orders')
    while iterations < config['opt_mode_conf']['rand_search']['stage2_iter']:
        pass_swap = list(best_order)
        idx = range(len(pass_swap))
        i1, i2 = random.sample(idx, 2)
        pass_swap[i1], pass_swap[i2] = pass_swap[i2], pass_swap[i1]
        config['opt']['passes'] = pass_swap
        instr = manual(config, False)
        iterations += 1
        if instructions > instr:
            best_order = list(pass_swap)
            instructions = instr
            iterations = 0

    print('Best optimization pass order')
    print('    Instructions     Order')
    line = [str(instructions), str(best_order)]
    print('    {: <16} {: <10}'.format(*line))
    print('Executing best order and printing instruction statistics')
    config['opt']['passes'] = best_order
    manual(config, True)


def preset(config):
    config['opt']['passes'] = [config['opt_mode_conf']['preset']['opt_level']]
    manual(config, True)


if __name__ == '__main__': main()
